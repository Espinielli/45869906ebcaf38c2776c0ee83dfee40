Fil has officially released support for polyhedral projections in `d3.geo`.
So this is here just for historical reasons.


This is a reproduction of my earlier test for
[Earth in a Cube I](https://bl.ocks.org/espinielli/9285de4dcd45ca2098561120461ea8cc) using Fil's
(not yet official) version of `d3-geo`.

Thanks to [Philippe Rivière](https://bl.ocks.org/Fil)'s great work
on `d3-geo` (see his
[PR for `clip-polygon`](https://github.com/d3/d3-geo/pull/108)),
now general polyhedral projections are possible in D3 v4!
(And let's not underestimate the great initial framework
implementation by [Jason Davies](https://www.jasondavies.com/maps/)!)
